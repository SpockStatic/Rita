import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Felulet extends JFrame {

    private JLabel jLabel01;
    private JLabel jLabel02;
    private JButton jButtonOpen01;
    private JButton jButtonOpen02;
    private JButton jStart;
    private JButton jMindketto;
    private JButton jOpen;
    private String path01;
    private String path02;
    String [] str01;
    String [] str02;


    public Felulet() {
        setLayout(null);

        jLabel01 = new JLabel("Eslő mappa:");
        jLabel01.setBounds(10,10,480,30);
        add(jLabel01);

        jButtonOpen01 = new JButton("Első mappa");
        jButtonOpen01.setBounds(80,40,140,30);
        add(jButtonOpen01);

        jLabel02 = new JLabel("Második mappa:");
        jLabel02.setBounds(10,70,480,30);
        add(jLabel02);

        jButtonOpen02 = new JButton("Második mappa");
        jButtonOpen02.setBounds(80,100,140,30);
        add(jButtonOpen02);

        jMindketto = new JButton("Mindekttőben");
        jMindketto.setBounds(80,150,140,30);
        add(jMindketto);

        jStart = new JButton("Külömbségek");
        jStart.setBounds(80,200,140,30);
        add(jStart);

        jOpen = new JButton("Utolsó eredmény");
        jOpen.setBounds(250,200,140,30);
        add(jOpen);


        jButtonOpen01.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                fcs1();
                jLabel01.setText(path01);

            }
        });


        jButtonOpen02.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                fcs2();
                jLabel02.setText(path02);

            }
        });

        jStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                  //  onlyGotOne(filenamesA(),filenamesB());
                    lastStep(onlyGotOne(filenamesA(),filenamesB()));
                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                }

            }
        });

        jMindketto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    //  onlyGotOne(filenamesA(),filenamesB());
                   lastStep(bothGotIt(filenamesA(),filenamesB()));
                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                }

            }
        });

        jOpen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                runFile();
            }
        });


    }
        public void fcs1 () {
            try {
                JFileChooser open = new JFileChooser("C:\\");
                int option = open.showOpenDialog(this);
                File f1 = new File(open.getCurrentDirectory().getPath());
                path01 = f1.getPath();

            } catch (Exception e) {
                System.out.println(e);
            }

        }

    public void fcs2 () {
        try {
            JFileChooser open = new JFileChooser("C:\\");

            int option = open.showOpenDialog(this);
            File f1 = new File(open.getCurrentDirectory().getPath());
            path02 = f1.getPath();


        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public String [] filenamesA()throws FileNotFoundException {


        File file = new File(path01);

        String [] strName = file.list();
        // get file name using getName()

        return strName;
    }

    public String [] filenamesB()throws FileNotFoundException {

        File file = new File(path02);

        String [] strName = file.list();
        // get file name using getName()

        return strName;
    }

    public ArrayList<String> onlyGotOne(String [] A, String [] B){

        ArrayList<String> ar = new ArrayList<String>();
        ar.add(" ");
        ar.add("--------------------------------------------");
        ar.add("A ("+path02+") -ben benne van");
        ar.add("("+path01+") mappából hiányzik");
        ar.add("--------------------------------------------");
        ar.add(" ");

       // System.out.println(path01+" mappában nem szerepel:");

        for(int i = 0; i < B.length; i++) {
            if (!Arrays.asList(A).contains(B[i])){
                ar.add(B[i]);
                /*System.out.println(B[i]);*/}


        }
        ar.add(" ");
        ar.add("--------------------------------------------");
        ar.add("A ("+path01+") -ben benne van");
        ar.add("("+path02+") mappából hiányzik");
        ar.add("--------------------------------------------");
        ar.add(" ");

       // System.out.println(path02+" mappában nem szerepel:");

        for(int i = 0; i < A.length; i++){
            if(!Arrays.asList(B).contains(A[i])){
                ar.add(A[i]);
                /*System.out.println(A[i]);*/}

        }

        return ar;

    }

    public ArrayList<String> bothGotIt(String [] A, String [] B){

        ArrayList<String> ar = new ArrayList<String>();
        ar.add(" ");
        ar.add("--------------------------------------------");
        ar.add("Mind a két mappában megtalálható");
        ar.add("--------------------------------------------");
        ar.add(" ");

        // System.out.println(path01+" mappában nem szerepel:");

        for(int i = 0; i < B.length; i++) {
            if (Arrays.asList(A).contains(B[i])){
                ar.add(B[i]);
                /*System.out.println(B[i]);*/}


        }


        return ar;

    }

    public void lastStep(ArrayList<String> list){
        try {
            // Creates a FileWriter
            File directory = new File("C:\\Eredmeny");

            if (!directory.exists()) {
                directory.mkdir();
                //System.out.println("Folder is created");

            } else {
                //System.out.println("Hiba, vagy már van ilyen mappa");
            }

            FileWriter file = new FileWriter("C:\\Eredmeny\\eredmeny.txt");
            // Creates a BufferedWriter
            BufferedWriter output = new BufferedWriter(file);

            // Writes the string to the file
            for(int i=0; i<list.size();i++) {
                output.write(list.get(i)+"\n");
            }

            JOptionPane.showMessageDialog(null,"A file elkészült a C:\\Eredmeny mappában");
            // Closes the writer
            output.close();

        }

        catch (Exception e) {
            e.getStackTrace();
        }

    }

    public void runFile(){
        try {
            Runtime.getRuntime().exec("notepad C:\\Eredmeny\\eredmeny");
        }catch (Exception e){
            JOptionPane.showMessageDialog(null,"Nincs előző eredmény");
        }


    }



    }


